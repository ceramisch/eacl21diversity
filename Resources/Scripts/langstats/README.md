# Language diversity statistics in ACL Anthology (titles and abstracts)

## Script `langdivstats2020.py`

The script takes as input:
 - the file containing the `.bib` items
 - the list of languages to look for, and 
 - the TAB-separated list of venues (acronym name)

It outputs a tsv table with the venues in rows and languages in columns.
Cells contain the number of items in the `.bib` file for each venue-language pair.
Language names are looked up (case-sensitive) in the title and abstract of bib items.

For EACL 2021 language diversity panel, the script was run as follows:
```bash
./langdivstats2020.py anthology+abstracts2020.bib langnames-clean.txt venues.txt
```
This can take a while, since the list of bib items and of languages is large and the script is not optimised.
The file `stats.tsv` contains the resulting TAB-separated table.
The tile `titleabstr.txt` contains a copy of the titles and abstracts to quickly check numbers using `grep`.

In addition to language names, we also give stats for:
- `#` - Total number of papers in that venue
- `None` - papers not mentioning any language name in title or abstract
- `Some` - Complement of `None`, papers mentioning at least 1 language in title or abstract
- `BERT` - papers mentioning BERT (contining the regexp `BERT|GPT|XLNet`)
- `onlyBERT` - Intersection of `BERT` and `None`, i.e. papers mentioning BERT but no language name
- `multilingual` - papers containing one of the terms below (these are included in `Some`, not in `None`)
```python
["Bilingual", "bilingual", "crosslingual", "Crosslingual", 
 "multilingual", "Multilingual", "Bi-lingual", "bi-lingual", 
 "cross-lingual", "Cross-lingual", "multi-lingual","Multi-lingual"]
```

## Criteria used to select language names in `langnames-clean.txt`

1. Download ISO 639 table with [language names and codes in UTF-8](https://iso639-3.sil.org/code_tables/download_tables)  
  * Version used: `iso-639-3_Code_Tables_20210218.zip`
2. Unzip, transform CR+LF into LF, get language names without extra fields:
```bash
unzip iso-639-3_Code_Tables_20210218.zip
fromdos iso-639-3_Code_Tables_20210218/iso-639-3_Name_Index.tab  
cat iso-639-3_Code_Tables_20210218/iso-639-3_Name_Index.tab | 
awk '{FS="\t";print $3}' | sed 's/, .*//g' | sed 's/ (.*//g' | 
tail -n +2 | sort | uniq > langnames-raw.txt
```
3. Some language names are homonyms with English words or author names. Some language names are only 1 or 2 letters long. They were manually removed in `langnames-clean.txt`. You can know which ones were removed by running:
```diff langnames-clean.txt langnames-raw.txt```

## Criteria used to create `venues.txt`

1. Include all journals in 2020 ACL Anthology (CL and TACL).
2. Include booktitles (proceedings of conferences, workshops) with more than 120 items:
```bash
grep "booktitle = " anthology+abstracts2020.bib | sort | uniq -c | sort -nr
```
3. Include official ACL events not included in the query above (AACL, CoNLL, *SEM).


Acronyms given as they appear on ACL Anthology home page.

## Criteria used to create `anthology+abstracts2020.bib`

1. Donwload the full anthology + abstracts from the website: https://aclanthology.org/anthology+abstracts.bib.gz
2. Manually remove events from years different from 2020

This is not necessary, as venues are filtered by the `venues.txt` file.
However, reading the whole `.bib` is slow, so this saves time.
