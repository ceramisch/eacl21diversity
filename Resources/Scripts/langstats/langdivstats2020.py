#!/usr/bin/env python3

import sys
import pybtex.database
from pylatexenc.latex2text import LatexNodes2Text
import pdb
from nltk.tokenize import word_tokenize
import re
from typing import List, Dict, Tuple, Set

"""
This script extracts from the ACL Anthology .bib the stats for language name mentions in titles and abstracts of papers (per venue).
"""

#########################################

def read_langs(langfilename:str) -> List[str]:
  """
  Returns a list of languages from a file.
  """  
  with open(langfilename) as langfile :
    langs = [langline.strip() for langline in langfile]
  return langs
  
#########################################  
  
def read_venues(venuesfilename:str) -> Dict[str,str]:
  """
  Returns a dictionary with conference name -> acronym from a file.
  """
  venues:Dict[str,str] = {}
  with open(venuesfilename) as venuesfile :
    for venueline in venuesfile :
      (acronym, fullname) = venueline.strip().split("\t")
      if fullname in venues :
        print("Warning: duplicated venue {}".format(fullname),file=sys.stderr)
      venues[fullname] = acronym
  return venues

#########################################

def is_interesting(ref:'pybtex.database.Entry')->bool:
  """
  Returns True if the item is a journal or conference paper and has an abstract.
  """
  return ref.type in ["inproceedings", "article"] and "abstract" in ref.fields

#########################################

def get_titleabstr(ref:'pybtex.database.Entry')->str:
  """
  Returns a dictionary with conference name -> acronym
  """
  title = LatexNodes2Text().latex_to_text(ref.fields["title"])
  abstr = LatexNodes2Text().latex_to_text(ref.fields["abstract"])
  titleabstr = " " + " ".join(word_tokenize(title + " " + abstr)) + " "
  titleabstr = re.sub("([^ ])-([^ ])","\g<1> - \g<2>", titleabstr)
  return re.sub(" ", "  ", titleabstr)

#########################################

def get_venue_acronym(venues:Dict[str,str], ref:'pybtex.database.Entry')->str:
  """
  Return the acronym of the venue of the reference.
  """  
  if "booktitle" in ref.fields :
    venue = LatexNodes2Text().latex_to_text(ref.fields["booktitle"])
  elif "journal" in ref.fields :
    venue = LatexNodes2Text().latex_to_text(ref.fields["journal"])
  else :
    print("No journal nor booktitle in {}".format(ref.key), file=sys.stderr)
    sys.exit(-1)
  if venue in venues :
    return venues[venue]
  else:
    return None  
  #  print("Venue not in list {}".format(venue), file=sys.stderr)
  #  sys.exit(-1)

#########################################

def print_titleabstr(allpapers:Dict[str,Tuple[str,Set[str]]])->None:
  with open("titleabstr.txt","w") as tafile :
    for (key,(ta,lang)) in allpapers.items() :
      print(key+"\t"+ta.strip().replace("  "," ")+"\t"+"|".join(lang),
            file=tafile)

#########################################

def print_venuestats(langstats:Dict[str,int], 
                     venuestats:Dict[str,Dict[str,Set[str]]])->None:
  with open("stats.tsv","w") as stats:
    colhead = ["X","#","None","Some","BERT","onlyBERT"]
    colhead += [k for (k,v) in sorted(langstats.items(),key=lambda x:x[1],
                                      reverse=True)]
    print("\t".join(colhead),file=stats)
    for (acro,values) in venuestats.items() :
      line = [acro]
      for head in colhead[1:] : # do not get "X"
        line.append(str(len(values.get(head,[]))))
      print("\t".join(line),file=stats)

#########################################
####   MAIN    ##########################
#########################################

if len(sys.argv) == 4 :
  anthofile = sys.argv[1]
  langsfile = sys.argv[2]
  venuefile = sys.argv[3]
else :
  usagestring = """Usage: {} <antho.bib> <langs.txt> <venue.txt>
  <antho.bib>: bib references from ACL anthology
  <langs.txt>: one language name per line
  <venue.txt>: venue acronym TAB name as in booktitle/journal"""
  print(usagestring.format(sys.argv[0]), file=sys.stderr)
  sys.exit(-1)

#########################################  

extra = ["Bilingual", "bilingual", "crosslingual", "Crosslingual", 
         "multilingual","Multilingual", "Bi-lingual", "bi-lingual", 
         "cross-lingual", "Cross-lingual", "multi-lingual","Multi-lingual"]
bertre = re.compile("BERT|GPT|XLNet")
langs = read_langs(langsfile) + extra
venues = read_venues(venuefile)
langpattern = re.compile("(" + " | ".join(langs) + ")")
anthology = pybtex.database.parse_file(anthofile)

# Counter for each language name                 
langstats:Dict[str,int] = {} 
# Dict per venue acronym, then counter for each language name
venuestats:Dict[str,Dict[str,Set[str]]] = {} 
# All papers' keys -> titles+abstracts
allpapers:Dict[str,Tuple[str,Set[str]]]  = {} 

for (refkey, refvalue) in anthology.entries.items() :  
  if is_interesting(refvalue):
    titleabstr = get_titleabstr(refvalue)
    venue_acronym = get_venue_acronym(venues, refvalue)
    if venue_acronym:      
      vstats = venuestats.get(venue_acronym, {"#":set([]), "None":set([]), 
                                              "Some":set([]), "BERT":set([]), 
                                              "onlyBERT":set([])})
      vstats["#"].add(refkey) # increase count of papers in this venue
      langsfound = langpattern.findall(titleabstr)      
      langlist = set([])
      hasbert = bertre.search(titleabstr)
      if hasbert :
        vstats["BERT"].add(refkey)
      if langsfound :
        for langfound in langsfound :
          langfound = langfound.strip()
          if langfound in extra:
            langfound = "multilingual"
          langlist.add(langfound)
          paperset = vstats.get(langfound, set([]))
          paperset.add(refkey)
          vstats[langfound] = paperset 
          vstats["Some"].add(refkey)     
      else :
        vstats["None"].add(refkey)
        if hasbert :
          vstats["onlyBERT"].add(refkey)
      allpapers[refkey] = (titleabstr, langlist)
      for lang in langlist :
        langstats[lang] = langstats.get(lang, 0) + 1
      venuestats[venue_acronym] = vstats
print_titleabstr(allpapers)
print_venuestats(langstats, venuestats)      
